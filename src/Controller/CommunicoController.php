<?php

namespace Drupal\communico\Controller;

use Drupal\Core\Controller\ControllerBase;

class CommunicoController extends ControllerBase {

  public function content() {
    $config = $this->config('communico.settings');

    $url = $config->get('url');

    return array(
      '#type' => 'markup',
      '#markup' => $this->t('Hello Everyone ' . $url),
    );
  }
}
