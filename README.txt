This Communico module provides both a simple plug and play event feeds integration with the Communico "Attend" product as well as a more generic API service.

The first feature, a simple feeds integration, is provided with a configurable block that allows feeds from Communico to be pulled in, displayed and themed in different regions of your site. These blocks are customizable by number of events, date ranges, and tags.

The second more advanced integration, the API service, is an abstracted Communico service that provides functions for GET/POST calls to the Communico API and handles the authentication steps and timeouts automatically.

To utilize this module you will need a Communico API access key and secret. Which must be entered on the main configuration page to utilize either feature of the module.

For more information about Communico, please see their website: https://communico.co/
